[![pipeline status](https://gitlab.com/openstreetcraft/timezone-service/badges/master/pipeline.svg)](https://gitlab.com/openstreetcraft/timezone-service/commits/master)

# Overview

Provides timezone information of a given location on Earth.

The following informations are supported:

* current time
* sunset
* sunrise

# Software Design

* RESTful webservice aggregating information from different third-party providers
* Provides a common interface to different services (facade pattern)
* Balances load, respects availability and service limits to be more resilient 

# Providers
 
The following providers are currently supported:

* [Geonames Webservice API](http://www.geonames.org/export/web-services.html#timezone)

More providers can be integrated as plugins by using a [service provider interface](https://en.wikipedia.org/wiki/Service_provider_interface) (SPI).

# Configuration

Some provider specific environment variables must be set to configure API keys.
A registration may be necessary to get an API key.

# Runtime

Service can be started with

```
./gradlew distDocker
docker-compose up
```

# REST API

REST API of a running service can be discovered with Swagger UI: 

http://localhost:8080/api/v1/swagger-ui.html

# Links

The project [openstreetcraft-api](https://gitlab.com/openstreetcraft/openstreetcraft-api)
is a Java library for easy access to this service.
