// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.timezone.provider.geonames;

import org.geonames.WebService;

import de.ixilon.microservices.timezone.model.Location;
import de.ixilon.microservices.timezone.model.Timezone;
import de.ixilon.microservices.timezone.spi.TimezoneProvider;
import de.ixilon.microservices.timezone.spi.TimezoneProviderException;

public class GeonamesTimezoneProvider implements TimezoneProvider {

  static {
    WebService.setUserName("openstreetcraft");
  }

  @Override
  public Timezone getTimezoneByLocation(Location location) throws TimezoneProviderException {
    try {
      return convert(WebService.timezone(location.getLatitude(), location.getLongitude()));
    } catch (Exception exception) {
      throw new TimezoneProviderException(exception);
    }
  }

  private static Timezone convert(org.geonames.Timezone timezone) {
    return new Timezone.Builder()
        .withTimezone(timezone.getTimezoneId())
        .withCurrentTime(timezone.getTime())
        .withSunrise(timezone.getSunrise())
        .withSunset(timezone.getSunset())
        .build();
  }
}
