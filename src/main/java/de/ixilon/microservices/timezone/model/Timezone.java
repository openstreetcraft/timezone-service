// Copyright (C) 2018 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.timezone.model;

import static java.util.Objects.requireNonNull;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class Timezone {
  private ZonedDateTime currentTime;
  private ZonedDateTime sunrise;
  private ZonedDateTime sunset;

  public String getCurrentTime() {
    return currentTime.toString();
  }

  public void setCurrentTime(String currentTime) {
    this.currentTime = ZonedDateTime.parse(currentTime);
  }

  public String getSunrise() {
    return sunrise.toString();
  }

  public void setSunrise(String sunrise) {
    this.sunrise = ZonedDateTime.parse(sunrise);
  }

  public String getSunset() {
    return sunset.toString();
  }

  public void setSunset(String sunset) {
    this.sunset = ZonedDateTime.parse(sunset);
  }

  public static class Builder {
    private ZoneId timezone;
    private Date currentTime;
    private Date sunrise;
    private Date sunset;

    /**
     * Create a Timezone instance.
     */
    public Timezone build() {
      Timezone timezone = new Timezone();

      timezone.currentTime = toZonedDateTime(requireNonNull(this.currentTime, "currentTime"));
      timezone.sunrise = toZonedDateTime(requireNonNull(this.sunrise, "sunrise"));
      timezone.sunset = toZonedDateTime(requireNonNull(this.sunset, "sunset"));

      return timezone;
    }

    private ZonedDateTime toZonedDateTime(Date date) {
      requireNonNull(timezone, "timezone");
      return ZonedDateTime.of(date.toInstant().atZone(timezone).toLocalDateTime(), timezone);
    }

    public Builder withTimezone(String timezone) {
      this.timezone = ZoneId.of(timezone);
      return this;
    }

    public Builder withCurrentTime(Date currentTime) {
      this.currentTime = new Date(currentTime.getTime());
      return this;
    }

    public Builder withSunrise(Date sunrise) {
      this.sunrise = new Date(sunrise.getTime());
      return this;
    }

    public Builder withSunset(Date sunset) {
      this.sunset = new Date(sunset.getTime());
      return this;
    }
  }
}
