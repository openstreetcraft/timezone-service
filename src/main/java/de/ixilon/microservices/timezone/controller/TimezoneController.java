// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.timezone.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import de.ixilon.microservices.timezone.model.Location;
import de.ixilon.microservices.timezone.model.Timezone;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * This controller defines the REST API endpoints.
 */
@RestController
public class TimezoneController {

  private final TimezoneService timezoneService;

  @Autowired
  public TimezoneController(TimezoneService timezoneService) {
    this.timezoneService = timezoneService;
  }

  @ApiOperation(value = "Get timezone info at location", response = Timezone.class)
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Successfully retrieved resource"),
      @ApiResponse(code = 404, message = "The resource is not found") })
  @RequestMapping(value = "/timezone", method = RequestMethod.GET, produces = "application/json")
  public ResponseEntity<Timezone> getTimezone(
      @RequestParam double longitude, @RequestParam double latitude) {
    Location location = Location.builder().withLongitude(longitude).withLatitude(latitude).build();
    Optional<Timezone> timezone = timezoneService.getTimezoneByLocation(location);
    if (timezone.isPresent()) {
      return new ResponseEntity<>(timezone.get(), HttpStatus.OK);
    }
    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
  }
}
