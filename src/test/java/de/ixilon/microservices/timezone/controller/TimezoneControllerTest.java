// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.microservices.timezone.controller;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import de.ixilon.microservices.timezone.model.Timezone;

public class TimezoneControllerTest {

  private TimezoneService service;
  private TimezoneController controller;

  @Before
  public void setUp() {
    service = new TimezoneService();
    controller = new TimezoneController(service);
  }

  @Test
  public void showsSample() {
    Timezone actual = controller.getTimezone(11.0, 12.0).getBody();
    assertNotNull(actual);
  }

}
